
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: "ZaplanujPrzejazd.pl",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: "Chcesz mieć wpływ na rozkład jazdy w swoim mieście? Zaplanuj przejazd komunikacją miejską i zadbaj o bezpieczeństwo swoje i innych podczas epidemii COVID-19" }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/tiv1twc.css' },
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
 css: [
  'element-ui/lib/theme-chalk/reset.css',
    'element-ui/lib/theme-chalk/index.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/element-ui'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    //'@nuxtjs/google-analytics'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
    'nuxt-vue-select',
    '@nuxtjs/google-gtag'
  ],
  'google-gtag': {
    id: 'UA-162759235-1'
  },
  axios: {
    proxy: true,
    credentials: false,
    baseURL: 'https://api.zaplanujprzejazd.pl'
  },
  proxy: {
    '/api/': { target: 'https://api.zaplanujprzejazd.pl', pathRewrite: {'^/api/': ''} }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
